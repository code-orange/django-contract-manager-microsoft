import requests


def get_tenant_id_by_domain(tenant_domain: str):
    response = requests.get(
        "https://login.windows.net/"
        + tenant_domain
        + "/.well-known/openid-configuration",
        headers={
            "Content-Type": "application/json",
        },
    )

    tenant_id = response.json()["token_endpoint"].split("/")[3]

    return tenant_id
